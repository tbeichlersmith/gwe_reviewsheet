# gwe_reviewsheet

An overview of important concepts and equations from the six major areas of physics that are put onto the UMN Physics GWE.

The six areas are
1. Classical Mechanics
2. Quantum Mechanics
3. Thermodynamics and Statistical Mechanics
4. Classical Electromagnetism
5. Nuclear and Particle Physics
6. Special Relativity

In addition to these six sections, there is also another section about "General Math" that is
useful in more than one of the above sections.
